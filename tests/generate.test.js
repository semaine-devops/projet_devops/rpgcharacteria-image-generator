import {generatePrompt} from "../pages/api/generate";
//const {generatePrompt} = require("../pages/api/generate");

test('prompt correctly generated', () => {
    const prompt = generatePrompt("female", "elf", "archer", "20");
    expect(prompt).toBe("Half body 3D render of a Dungeons & Dragons 20 years old female archer elf");
});